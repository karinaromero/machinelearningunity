﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

// Class to store past states for actions and ssociated rewards
public class Replay
{
    public List<double> states;
    public double reward;

    public Replay(double disTop, double disBottom, double r)
    {
        states = new List<double>();

        states.Add(disTop);
        states.Add(disBottom);

        reward = r;
    }
}

public class Brain : MonoBehaviour
{
    // Limits TOP/BOTTOM
    public GameObject topLimit;
    public GameObject bottomLimit;

    ANN ann;

    // Reward to associate with actions
    float reward = 0.0f;
    // Memory - list of past actions and rewards
    List<Replay> replayMemory = new List<Replay>();
    // Memory capacity
    int mCapacity = 10000;

    // How much future states affect rewards
    float discount = 0.99f;
    // Chance of picking random actions
    float exploreRate = 100.0f;
    // Max chance value
    float maxExploreRate = 100.0f;
    // Min chance value
    float minExploreRate = 0.01f;
    // Chance decay amount for each update
    float exploreDecay = 0.0001f;

    // Record start position of object
    Vector3 dragonStartPosition;
    // count when the ball is dropped
    int failCount = 0;
    float moveForce = 0.5f;

    float timer = 0;
    float maxBalanceTime = 0;

    bool crashed = false;
    public Rigidbody2D rigidbodyDragon;

    GUIStyle gUIStyle = new GUIStyle();

    private void Start()
    {
        ann = new ANN(2, 2, 1, 6, 0.2f);
        dragonStartPosition = this.transform.position;
        Time.timeScale = 5.0f;
        rigidbodyDragon = this.gameObject.GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            ResetDragon();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        crashed = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        crashed = false;
    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        List<double> states = new List<double>();
        List<double> qs = new List<double>();

        states.Add(Vector3.Distance(this.transform.position, topLimit.transform.position));
        states.Add(Vector3.Distance(this.transform.position, bottomLimit.transform.position));

        qs = SoftMax(ann.CalcOutput(states));
        double maxQ = qs.Max();
        int maxQIndex = qs.ToList().IndexOf(maxQ);
        exploreRate = Mathf.Clamp(exploreRate - exploreDecay, minExploreRate, maxExploreRate);

        /*if (Random.Range(0, 100) < exploreRate)
        {
            maxQIndex = Random.Range(0, 2);
        }*/
        if (maxQIndex == 0)
        {
            rigidbodyDragon.AddForce(Vector3.up * moveForce * (float)qs[maxQIndex]);
        }
        else if (maxQIndex == 1)
        {
            rigidbodyDragon.AddForce(Vector3.up * -moveForce * (float)qs[maxQIndex]);
        }

        if (crashed)
        {
            reward = -1.0f;
        }
        else
        {
            reward = 0.1f;
        }

        Replay lastMemory = new Replay(Vector3.Distance(this.transform.position, topLimit.transform.position),
                                        Vector3.Distance(this.transform.position, bottomLimit.transform.position),
                                        reward);

        if (replayMemory.Count > mCapacity)
        {
            replayMemory.RemoveAt(0);
        }

        replayMemory.Add(lastMemory);

        if (crashed)
        {
            for (int i = replayMemory.Count - 1; i >= 0; i--)
            {
                List<double> toutputsOld = new List<double>();
                List<double> toutputsNew = new List<double>();

                toutputsOld = SoftMax(ann.CalcOutput(replayMemory[i].states));

                double maxQOld = toutputsOld.Max();
                int action = toutputsOld.ToList().IndexOf(maxQOld);


                double feedback;
                if (i == replayMemory.Count - 1 || replayMemory[i].reward == -1)
                {
                    feedback = replayMemory[i].reward;
                }
                else
                {
                    toutputsNew = SoftMax(ann.CalcOutput(replayMemory[i+1].states));
                    maxQ = toutputsNew.Max();
                    feedback = (replayMemory[i].reward + discount * maxQ);
                }

                toutputsOld[action] = feedback;
                ann.Train(replayMemory[i].states, toutputsOld);
            }

            if (timer > maxBalanceTime)
            {
                maxBalanceTime = timer;
            }

            timer = 0;
            crashed = false;
            ResetDragon();
            replayMemory.Clear();
            failCount++;
        }
    }

    List<double> SoftMax(List<double> values)
    {
        double max = values.Max();
        float scale = 0.0f;

        for (int i = 0; i < values.Count; ++i)
        {
            scale += Mathf.Exp((float)(values[i] - max));
        }

        List<double> result = new List<double>();

        for (int i = 0; i < values.Count; ++i)
        {
            result.Add(Mathf.Exp((float)(values[i] - max)) / scale);
        }
        return result;
    }

    private void OnGUI()
    {
        gUIStyle.fontSize = 25;
        gUIStyle.normal.textColor = Color.white;

        GUI.BeginGroup(new Rect(10, 10, 600, 150));
        GUI.Box(new Rect(0, 0, 140, 140), "Stats: ", gUIStyle);
        GUI.Label(new Rect(10, 25, 500, 30), "Fails: " + failCount, gUIStyle);
        GUI.Label(new Rect(10, 50, 500, 30), "Decay Rate: " + exploreRate, gUIStyle);
        GUI.Label(new Rect(10, 75, 500, 30), "Last Best Balance: " + maxBalanceTime, gUIStyle);
        GUI.Label(new Rect(10, 100, 500, 30), "This Balance: " + timer, gUIStyle);
        GUI.EndGroup();
    }

    void ResetDragon()
    {
        this.transform.position = dragonStartPosition;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
    }
}
