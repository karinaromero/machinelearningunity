﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA : MonoBehaviour
{
    private List<int> _genes = new List<int>();
    private int _dnaLength = 0;
    private int _maxValues = 0;

    public DNA(int l, int v)
    {
        _dnaLength = l;
        _maxValues = v;
        SetRandom();
    }

    public void SetRandom()
    {
        _genes.Clear();

        for(int i = 0; i < _dnaLength; i++)
        {
            _genes.Add(Random.Range(0, _maxValues));
        }
    }

    public void SetInt(int pos, int value)
    {
        _genes[pos] = value;
    }

    public void Combine(DNA d1, DNA d2)
    {
        for (int i = 0; i < _dnaLength; i++)
        {
            if(i < _dnaLength / 2.0)
            {
                int c = d1._genes[i];
                _genes[i] = c;
            }
            else
            {
                int c = d2._genes[i];
                _genes[i] = c;
            }
        }
    }

    public void Mutate()
    {
        _genes[Random.Range(0, _dnaLength)] = Random.Range(0, _maxValues);
    }

    public int GetGene(int pos)
    {
        return _genes[pos];
    }
}
