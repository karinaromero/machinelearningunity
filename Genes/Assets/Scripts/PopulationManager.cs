﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PopulationManager : MonoBehaviour
{
    public GameObject botPrefab;
    public int populationSize = 50;
    public static float elapsed = 0;
    public float trialTime = 5;
    public float distanceTravel;
    
    private int _generation = 1;
    private List<GameObject> _population = new List<GameObject>();
    private GUIStyle _gUIStyle = new GUIStyle();

    private void OnGUI()
    {
        _gUIStyle.fontSize = 25;
        _gUIStyle.normal.textColor = Color.yellow;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));
        GUI.Box(new Rect(0, 0, 140, 140), "Stats", _gUIStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Generation: " + _generation, _gUIStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time: {0:0.00}", elapsed), _gUIStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + _population.Count, _gUIStyle);
        GUI.EndGroup();
    }



    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < populationSize; i++)
        {
            Vector3 startingPosition = new Vector3(this.transform.position.x + Random.Range(-2, 2),
                                                    this.transform.position.y,
                                                    this.transform.position.z + Random.Range(-2, 2));
            GameObject b = Instantiate(botPrefab, startingPosition, this.transform.rotation);
            b.GetComponent<Brain>().Init();
            _population.Add(b);
        }
    }

    private GameObject Breed(GameObject parent1, GameObject parent2)
    {
        Vector3 startingPosition = new Vector3(this.transform.position.x + Random.Range(-2, 2),
                                                    this.transform.position.y,
                                                    this.transform.position.z + Random.Range(-2, 2));
        GameObject offSpring = Instantiate(botPrefab, startingPosition, this.transform.rotation);
        Brain b = offSpring.GetComponent<Brain>();

        if(Random.Range(0, 100) == 1)
        {
            b.Init();
            b.dna.Mutate();
        }
        else
        {
            b.Init();
            b.dna.Combine(parent1.GetComponent<Brain>().dna, parent2.GetComponent<Brain>().dna);
        }

        return offSpring;
    }

    private void BreedNewPopulation()
    {
        // To calculate using time alive
        //List<GameObject> sortedList = _population.OrderBy(o => o.GetComponent<Brain>().timeAlive).ToList();

        // To calculate using distances travel
        List<GameObject> sortedList = _population.OrderBy(o => o.GetComponent<Brain>().distanceTravel).ToList();

        _population.Clear();
        for(int i = (int)(sortedList.Count/2.0f); i < sortedList.Count-1; i++)
        {
            _population.Add(Breed(sortedList[i], sortedList[i + 1]));
            _population.Add(Breed(sortedList[i + 1], sortedList[i]));
        }
        for(int i = 0; i < sortedList.Count; i++)
        {
            Destroy(sortedList[i]);
        }
        _generation++;
    } 

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if(elapsed >= trialTime)
        {
            BreedNewPopulation();
            elapsed = 0;
        }
    }
}
