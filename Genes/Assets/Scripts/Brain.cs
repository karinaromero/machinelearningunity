﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;


[RequireComponent(typeof(ThirdPersonCharacter))]
public class Brain : MonoBehaviour
{
    public int DNALength = 1;
    public float timeAlive;
    public DNA dna;
    public float distanceTravel;
    Vector3 _startPosition;

    private ThirdPersonCharacter m_Character;
    private Vector3 m_Move;
    private bool m_Jump;
    private bool _alive = true;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "dead")
        {
            _alive = false;
        }
    }

    public void Init()
    {
        dna = new DNA(DNALength, 6);
        m_Character = GetComponent<ThirdPersonCharacter>();
        timeAlive = 0;
        _alive = true;
        _startPosition = this.transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float h = 0;
        float v = 0;
        bool crouch = false;

        switch (dna.GetGene(0))
        {
            case 0:
                v = 1; 
                break;
            case 1:
                v = -1;
                break;
            case 2:
                h = -1;
                break;
            case 3:
                h = 1;
                break;
            case 4:
                m_Jump = true;
                break;
            case 5:
                crouch = true;
                break;
        }
        m_Move = v * Vector3.forward + h * Vector3.right;
        m_Character.Move(m_Move, crouch, m_Jump);
        m_Jump = false;

        if (_alive)
        {
            timeAlive += Time.deltaTime;
            distanceTravel = Vector3.Distance(this.transform.position, _startPosition);
        }
    }
}
