﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveBall : MonoBehaviour 
{

    public Text playerText;
    public Text iaText;

    public int scorePlayer = 0;
    public int scoreAI = 0;

    Vector3 ballStartPosition;
	Rigidbody2D _rigidbody2D;
    AudioSource _audioSource;
	
    float _speed = 300;

	public AudioClip blip;
	public AudioClip blop;



    private void Start()
    {
        _audioSource = this.GetComponent<AudioSource>();
        _rigidbody2D = this.GetComponent<Rigidbody2D>();
		ballStartPosition = this.transform.position;
		ResetBall();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "backplayerwall")
        {
            _audioSource.PlayOneShot(blop);
            scoreAI++;
            UpdateScores();
        }
        else if (collision.gameObject.tag == "backwall")
        {
            _audioSource.PlayOneShot(blop);
            scorePlayer++;
            UpdateScores();
        }
        else
        {
            _audioSource.PlayOneShot(blip);
        }
    }

    public void ResetBall()
    {
        this.transform.position = ballStartPosition;
        _rigidbody2D.velocity = Vector3.zero;
        Vector3 dir = new Vector3(Random.Range(100, 300), Random.Range(-100, 100), 0).normalized;
        _rigidbody2D.AddForce(dir*_speed);
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            ResetBall();
        }
    }

    private void UpdateScores()
    {
        playerText.text = scorePlayer.ToString();
        iaText.text = scoreAI.ToString();
    }
}
