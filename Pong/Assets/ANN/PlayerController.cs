﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    float paddleMaxSpeed = 12.0f;
    Rigidbody2D _rigidbody2D;
    public float directionY;

    private void Start()
    {
        _rigidbody2D = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        directionY = Input.GetAxis("Vertical") * paddleMaxSpeed;
    }

    private void FixedUpdate()
    {
         _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, directionY);
    }
}
