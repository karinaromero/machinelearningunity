﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Replay
{
    public List<double> states;
    public double reward;

    public Replay(double xr, double ballz, double ballvz, double r)
    {
        states = new List<double>();

        states.Add(xr);
        states.Add(ballz);
        states.Add(ballvz);

        reward = r;
    }
}

public class Brain : MonoBehaviour
{
    // Object monitor
    public GameObject ball;

    // Reward to associate with actions
    float reward = 0.0f;
    // Memory - list of past actions and rewards
    List<Replay> replayMemory = new List<Replay>();
    // Memory capacity
    int mCapacity = 10000;

    // How much future states affect rewards
    float discount = 0.99f;
    // Chance of picking random actions
    float exploreRate = 100.0f;
    // Max chance value
    float maxExploreRate = 100.0f;
    // Min chance value
    float minExploreRate = 0.01f;
    // Chance decay amount for each update
    float exploreDecay = 0.0001f;

    // Record start position of object
    Vector3 ballStartPosition;
    // count when the ball is dropped
    int failCount = 0;
    /*
     * max angle to apply to tilting each update
     * make sure this is large enough to recover balance
     * multiplied by it is enough to recover balance
     * when the ball gets a good speed up
     * timer to keep track of balancing 
     * record time ball is kept balanced
     */
    float tiltSpeed = 0.5f;

    float timer = 0;
    float maxBalanceTime = 0;

    ANN ann;

    GUIStyle gUIStyle = new GUIStyle();

    private void Start()
    {
        ann = new ANN(3, 2, 1, 6, 0.2f);
        ballStartPosition = ball.transform.position;
        Time.timeScale = 5.0f;
    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            ResetBall();
        }
    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;
        List<double> states = new List<double>();
        List<double> qs = new List<double>();

        states.Add(this.transform.rotation.x);
        states.Add(ball.transform.position.z);
        states.Add(ball.GetComponent<Rigidbody>().angularVelocity.x);

        qs = SoftMax(ann.CalcOutput(states));
        double maxQ = qs.Max();
        int maxQIndex = qs.ToList().IndexOf(maxQ);
        exploreRate = Mathf.Clamp(exploreRate - exploreDecay, minExploreRate, maxExploreRate);

        /*if (Random.Range(0, 100) < exploreRate)
        {
            maxQIndex = Random.Range(0, 2);
        }*/
        if (maxQIndex == 0)
        {
            this.transform.Rotate(Vector3.right, tiltSpeed * (float)qs[maxQIndex]);
        }
        else if (maxQIndex == 1)
        {
            this.transform.Rotate(Vector3.right, -tiltSpeed * (float)qs[maxQIndex]);
        }

        if (ball.GetComponent<BallState>().dropped)
        {
            reward = -1.0f;
        }
        else
        {
            reward = 0.1f;
        }

        Replay lastMemory = new Replay(this.transform.rotation.x,
                                        ball.transform.position.z,
                                        ball.GetComponent<Rigidbody>().angularVelocity.x,
                                        reward);

        if (replayMemory.Count > mCapacity)
        {
            replayMemory.RemoveAt(0);
        }

        replayMemory.Add(lastMemory);

        if (ball.GetComponent<BallState>().dropped)
        {
            for (int i = replayMemory.Count - 1; i >= 0; i--)
            {
                List<double> toutputsOld = new List<double>();
                List<double> toutputsNew = new List<double>();

                toutputsOld = SoftMax(ann.CalcOutput(replayMemory[i].states));

                double maxQOld = toutputsOld.Max();
                int action = toutputsOld.ToList().IndexOf(maxQOld);


                double feedback;
                if (i == replayMemory.Count - 1 || replayMemory[i].reward == -1)
                {
                    feedback = replayMemory[i].reward;
                }
                else
                {
                    toutputsNew = SoftMax(ann.CalcOutput(replayMemory[i+1].states));
                    maxQ = toutputsNew.Max();
                    feedback = (replayMemory[i].reward + discount * maxQ);
                }

                toutputsOld[action] = feedback;
                ann.Train(replayMemory[i].states, toutputsOld);
            }

            if (timer > maxBalanceTime)
            {
                maxBalanceTime = timer;
            }

            timer = 0;
            ball.GetComponent<BallState>().dropped = false;
            this.transform.rotation = Quaternion.identity;
            ResetBall();
            replayMemory.Clear();
            failCount++;
        }
    }

    List<double> SoftMax(List<double> values)
    {
        double max = values.Max();
        float scale = 0.0f;

        for (int i = 0; i < values.Count; ++i)
        {
            scale += Mathf.Exp((float)(values[i] - max));
        }

        List<double> result = new List<double>();

        for (int i = 0; i < values.Count; ++i)
        {
            result.Add(Mathf.Exp((float)(values[i] - max)) / scale);
        }
        return result;
    }

    private void OnGUI()
    {
        gUIStyle.fontSize = 25;
        gUIStyle.normal.textColor = Color.white;

        GUI.BeginGroup(new Rect(10, 10, 600, 150));
        GUI.Box(new Rect(0, 0, 140, 140), "Stats: ", gUIStyle);
        GUI.Label(new Rect(10, 25, 500, 30), "Fails: " + failCount, gUIStyle);
        GUI.Label(new Rect(10, 50, 500, 30), "Decay Rate: " + exploreRate, gUIStyle);
        GUI.Label(new Rect(10, 75, 500, 30), "Last Best Balance: " + maxBalanceTime, gUIStyle);
        GUI.Label(new Rect(10, 100, 500, 30), "This Balance: " + timer, gUIStyle);
        GUI.EndGroup();
    }

    void ResetBall()
    {
        ball.transform.position = ballStartPosition;
        ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
        ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
    }
}
