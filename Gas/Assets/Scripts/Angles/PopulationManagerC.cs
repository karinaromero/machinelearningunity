﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PopulationManagerC : MonoBehaviour
{
    public GameObject botPrefab;
    public GameObject startPosition;
    public int populationSize = 50;
    List<GameObject> population = new List<GameObject>();
    public static float elapsed = 0;
    public float trialTime = 5;
    int generation = 1;

    GUIStyle guiStyle = new GUIStyle();

    private void OnGUI()
    {
        guiStyle.fontSize = 25;
        guiStyle.normal.textColor = Color.white;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));

        GUI.Box(new Rect(0, 0, 140, 140), "Stats ", guiStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Gen: " + generation, guiStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time: {0:0.00}", elapsed), guiStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + population.Count, guiStyle);

        GUI.EndGroup();
    }

    private void Start()
    {
        for (int i = 0; i < populationSize; i++)
        {
            GameObject b = Instantiate(botPrefab, startPosition.transform.position, this.transform.rotation);
            b.GetComponent<BrainC>().Init();
            population.Add(b);
        }
    }

    GameObject Breed(GameObject parent1, GameObject parent2)
    {
        GameObject offSpring = Instantiate(botPrefab, startPosition.transform.position, this.transform.rotation);
        BrainC brain = offSpring.GetComponent<BrainC>();
        if (Random.Range(0, 100) == 1) //  Mutate 1 in 100
        {
            brain.Init();
            brain.dna.Mutate();
        }
        else
        {
            brain.Init();
            brain.dna.Combine(parent1.GetComponent<BrainC>().dna, parent2.GetComponent<BrainC>().dna);
        }
        return offSpring;
    }
    void BreedNewPopulation()
    {
        List<GameObject> sortedList = population.OrderBy(o => (o.GetComponent<BrainC>().distanceTravelled)).ToList();

        //Debug.Log("VALUE OF I " + sortedList.Count + " |  Gen " + generation);

        population.Clear();
        // Breed upper half of sorted list
        for (int i = (int)(sortedList.Count / 2.0f); i < sortedList.Count - 1; i++)
        {
            //Debug.Log("VALUE OF I "+ i + " |  Gen " + generation );
            population.Add(Breed(sortedList[i], sortedList[i + 1]));
            population.Add(Breed(sortedList[i + 1], sortedList[i]));
        }
        // Destroy all parents and previous population
        for (int i = 0; i < sortedList.Count; i++)
        {
            Destroy(sortedList[i]);
        }
        generation++;
    }

    private void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= trialTime)
        {
            BreedNewPopulation();
            elapsed = 0;
        }
    }
}
