﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainC : MonoBehaviour
{
    int DNALength = 2;
    public DNAC dna;

    public GameObject eyes;

    bool seeWall = true;
    Vector3 startPosition;
    public float distanceTravelled = 0;
    bool alive = true;

    public GameObject ethanPrefab;
    GameObject ethan;

    public void Init()
    {
        // Initialize DNA
        // 0 forward
        // 1 left
        // 2 right
        dna = new DNAC(DNALength, 360);
        startPosition = this.transform.position;

        ethan = Instantiate(ethanPrefab, this.transform.position, this.transform.rotation);
        ethan.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().target = this.transform;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "dead")
        {
            distanceTravelled = 0;
            alive = false;
        }
    }

    private void OnDestroy()
    {
        Destroy(ethan);
    }

    private void Update()
    {
        if (!alive)
        {
            return;
        }
        Debug.DrawRay(eyes.transform.position, eyes.transform.forward * 0.5f, Color.red);
        seeWall = false;
        RaycastHit hit;
        if (Physics.SphereCast(eyes.transform.position, 0.1f, eyes.transform.forward, out hit, 0.5f))
        {
            if (hit.collider.gameObject.tag == "wall")
            {
                seeWall = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (!alive)
        {
            return;
        }
        // read DNA
        float h = 0;
        float v = dna.GetGene(0);

        if (seeWall)
        {
            h = dna.GetGene(1);
        }

        this.transform.Translate(0, 0, v * 0.0001f);
        this.transform.Rotate(0, h, 0);
        distanceTravelled = Vector3.Distance(startPosition, this.transform.position);
    }
}
