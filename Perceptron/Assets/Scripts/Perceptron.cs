﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrainingSet
{
    public double[] input;
    public double output;
}

public class Perceptron : MonoBehaviour
{
    public TrainingSet[] trainingSet;

    double[] _weights = { 0,0 };
    double bias = 0;
    double totalerror = 0;

    public SimpleGrapher simpleGrapher;

    void InitialiseWeights()
    {
        for(int i = 0; i < _weights.Length; i++)
        {
            _weights[i] = Random.Range(-1.0f, 1.0f);
        }
        bias = Random.Range(-1.0f, 1.0f);
    }

    void Train(int epochs)
    {
        InitialiseWeights();

        for (int e = 0; e < epochs; e++)
        {
            totalerror = 0;
            for(int t = 0; t < trainingSet.Length; t++)
            {
                UpdateWeights(t);
                Debug.Log("Weight 1: " + (_weights[0]) + " Weight 1: " + (_weights[1]) + " Bias: " + bias);
            }
            Debug.Log("Total error: " + totalerror);
        }
    }

    void UpdateWeights(int trainingSetPosition)
    {
        double error = trainingSet[trainingSetPosition].output - CalcOutput(trainingSetPosition);

        totalerror += Mathf.Abs((float)error);

        for (int i = 0; i < _weights.Length; i++)
        {
            _weights[i] = _weights[i] + error * trainingSet[trainingSetPosition].input[i];
        }
        bias += error;
    }

    double DotProductBias(double[] v1, double[] v2)
    {
        if(v1 == null || v2 == null)
        {
            return -1;
        }

        if (v1.Length != v2.Length)
        {
            return -1;
        }

        double dot = 0;

        for (int x = 0; x < v1.Length; x++)
        {
            dot += v1[x] * v2[x];
        }

        dot += bias;

        return dot;
    }

    double CalcOutput(int trainingSetIndex)
    {
        double dotProductBias = DotProductBias(_weights, trainingSet[trainingSetIndex].input);

        if(dotProductBias > 0)
        {
            return (1);
        }

        return (0);
    }

    double CalcOutput(double input1, double input2)
    {
        double[] inputValues = new double[] { input1, input2 };

        double dotProductBias = DotProductBias(_weights, inputValues);

        if (dotProductBias > 0)
        {
            return (1);
        }

        return (0);
    }

    void DrawAllPoints()
    {
        for (int t = 0; t < trainingSet.Length; t++)
        {
            if(trainingSet[t].output == 0)
            {
                simpleGrapher.DrawPoint((float)trainingSet[t].input[0], (float)trainingSet[t].input[1], Color.magenta);
            }
            else
            {
                simpleGrapher.DrawPoint((float)trainingSet[t].input[0], (float)trainingSet[t].input[1], Color.green);
            }
        }
    }

    private void Start()
    {
        DrawAllPoints();
        Train(200);
        simpleGrapher.DrawRay((float)(-(bias/_weights[1])/(bias/_weights[0])), (float)(-bias/_weights[1]), Color.red);
        /*Debug.Log("Test 0 0: " + CalcOutput(0, 0));
        Debug.Log("Test 0 1: " + CalcOutput(0, 1));
        Debug.Log("Test 1 0: " + CalcOutput(1, 0));
        Debug.Log("Test 1 1: " + CalcOutput(1, 1));*/

        if (CalcOutput(0.3, 0.9) == 0)
        {
            simpleGrapher.DrawPoint(0.3f, 0.9f, Color.blue);
        }
        else
        {
            simpleGrapher.DrawPoint(0.3f, 0.9f, Color.yellow);
        }

        if (CalcOutput(0.8, 0.1) == 0)
        {
            simpleGrapher.DrawPoint(0.8f, 0.1f, Color.blue);
        }
        else
        {
            simpleGrapher.DrawPoint(0.8f, 0.1f, Color.yellow);
        }
    }
}
