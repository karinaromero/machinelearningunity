﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PopulationManager : MonoBehaviour
{
    public GameObject botPrefab;
    public GameObject startingPosition;

    public int populationSize = 150;

    List<GameObject> population = new List<GameObject>();

    public static float elapsed = 0;
    public float trialTime = 20;
    int generation = 1;

    GUIStyle gUIStyle = new GUIStyle();

    private void OnGUI()
    {
        gUIStyle.fontSize = 25;
        gUIStyle.normal.textColor = Color.white;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));

        GUI.Box(new Rect(0, 0, 140, 140), "Stats ", gUIStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Gen: " + generation, gUIStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time: {0:0.00}", elapsed), gUIStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + population.Count, gUIStyle);

        GUI.EndGroup();
    }

    private void Start()
    {
        for (int i = 0; i < populationSize; i++)
        {
            GameObject dragon = Instantiate(botPrefab, startingPosition.transform.position, this.transform.rotation);
            dragon.GetComponent<Brain>().Init();
            population.Add(dragon);
        }
    }

    GameObject Breed(GameObject parent1, GameObject parent2)
    {
        GameObject offSpring = Instantiate(botPrefab, startingPosition.transform.position, this.transform.rotation);
        Brain brain = offSpring.GetComponent<Brain>();

        if (Random.Range(0, 100) == 1) // Mutate 1 in 100
        {
            brain.Init();
            brain.dna.Mutate();
        }
        else
        {
            brain.Init();
            brain.dna.Combine(parent1.GetComponent<Brain>().dna, parent1.GetComponent<Brain>().dna);
        }

        return offSpring;
    }

    void BreedNewPopulation()
    {
        List<GameObject> sortedList = population.OrderBy(o => o.GetComponent<Brain>().distanceTraveled - o.GetComponent<Brain>().crash).ToList();

        population.Clear();

        for (int i = (int)(3 * sortedList.Count/4.0f); i < sortedList.Count-1; i++)
        {
            population.Add(Breed(sortedList[i], sortedList[i + 1]));
            population.Add(Breed(sortedList[i + 1], sortedList[i]));
            population.Add(Breed(sortedList[i], sortedList[i + 1]));
            population.Add(Breed(sortedList[i + 1], sortedList[i]));
        }

        // Destroy all parents and previous population
        for (int i = 0; i < sortedList.Count - 1; i++)
        {
            Destroy(sortedList[i]);
        }
        generation++;
    }

    private void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= trialTime)
        {
            BreedNewPopulation();
            elapsed = 0;
        }
    }
}
