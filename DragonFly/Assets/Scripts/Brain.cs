﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brain : MonoBehaviour
{
    int _DNALength = 5;

    public DNA dna;
    public GameObject eyes;

    bool _seeDownWall = false;
    bool _seeUpWall = false;
    bool _seeBottom = false;
    bool _seeTop = false;

    Vector3 _startPosition;

    public float timeAlive = 0;
    public float distanceTraveled = 0;
    public int crash = 0;

    bool _alive = true;
    Rigidbody2D _rigidbody;

    public void Init()
    {
        // Initialize DNA
        // 0 forward
        // 1 upwall
        // 2 downwall
        // 3 normal upward
        dna = new DNA(_DNALength, 200);
        this.transform.Translate(Random.Range(-1.5f, 1.5f), Random.Range(-1.5f, 1.5f), 0);
        _startPosition = this.transform.position;
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "top" ||
            collision.gameObject.tag == "bottom" ||
            collision.gameObject.tag == "upwall" ||
            collision.gameObject.tag == "downwall")
        {
            crash++;
        }
        else if (collision.gameObject.tag == "dead")
        {
            _alive = false;
        }
    }

    private void Update()
    {
        if (!_alive)
        {
            return;
        }

        _seeDownWall = false;
        _seeUpWall = false;
        _seeBottom = false;
        _seeTop = false;

        RaycastHit2D hit = Physics2D.Raycast(eyes.transform.position, eyes.transform.forward, 3.0f);

        Debug.DrawRay(eyes.transform.position, eyes.transform.forward * 3.0f, Color.red);
        Debug.DrawRay(eyes.transform.position, eyes.transform.up * 3.0f, Color.red);
        Debug.DrawRay(eyes.transform.position, -eyes.transform.up * 3.0f, Color.red);

        if (hit.collider != null)
        {
            if(hit.collider.tag == "upwall")
            {
                _seeUpWall = true;
            }
            else if (hit.collider.tag == "downwall")
            {
                _seeDownWall = true;
            }
        }

        hit = Physics2D.Raycast(eyes.transform.position, eyes.transform.up, 3.0f);

        if (hit.collider != null)
        {
            if (hit.collider.tag == "top")
            {
                _seeTop = true;
            }
        }

        hit = Physics2D.Raycast(eyes.transform.position, eyes.transform.up, 3.0f);

        if (hit.collider != null)
        {
            if (hit.collider.tag == "bottom")
            {
                _seeBottom = true;
            }
        }
        timeAlive = PopulationManager.elapsed;
    }

    private void FixedUpdate()
    {
        if (!_alive)
        {
            return;
        }

        // Read DNA
        float upForce = 0;
        float forwardForce = 1.0f;

        if (_seeUpWall)
        {
            upForce = dna.GetGene(0);
        }
        else if (_seeDownWall)
        {
            upForce = dna.GetGene(1);
        }
        else if (_seeTop)
        {
            upForce = dna.GetGene(2);
        }
        else if (_seeBottom)
        {
            upForce = dna.GetGene(3);
        }
        else
        {
            upForce = dna.GetGene(4);
        }

        _rigidbody.AddForce(this.transform.right * forwardForce);
        _rigidbody.AddForce(this.transform.up * upForce * 0.1f);
        distanceTraveled = Vector3.Distance(_startPosition, this.transform.position);
    }
}
