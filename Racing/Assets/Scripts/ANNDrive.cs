﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ANNDrive : MonoBehaviour
{
    ANN _ann;

    public float visibleDistance = 50;
    public int epochs = 1000;
    public float speed = 50.0F;
    public float rotationSpeed = 100.0f;


    bool _trainingDone = false;
    float _trainingProgress = 0;
    double _sse = 0;
    double _lastSEE = 1;

    public float translation;
    public float rotation;

    public bool loadFromFile = false;

    private void Start()
    {
        _ann = new ANN(5, 2, 1, 10, 0.5);

        if (loadFromFile)
        {
            LoadWeightsFromFile();
            _trainingDone = true;
        }
        else
        {
            StartCoroutine(LoadTrainingSet());
        }
    }

    IEnumerator LoadTrainingSet()
    {
        string path = Application.dataPath + "/trainigData.txt";
        string line;

        if(File.Exists(path))
        {
            int lineCount = File.ReadAllLines(path).Length;
            StreamReader tdf = File.OpenText(path);

            List<double> calcOutputs = new List<double>();
            List<double> inputs = new List<double>();
            List<double> outputs = new List<double>();

            for (int i = 0; i < epochs; i++)
            {
                // See file pointer to beginning of file
                _sse = 0;
                tdf.BaseStream.Position = 0;
                string currentWeights = _ann.PrintWeights();
                while ((line = tdf.ReadLine()) != null)
                {
                    string[] data = line.Split('/');
                    // If nothing to be learned ignore this line
                    float thisError = 0;

                    if (System.Convert.ToDouble(data[5]) != 0 && System.Convert.ToDouble(data[6]) != 0)
                    {
                        inputs.Clear();
                        outputs.Clear();

                        inputs.Add(System.Convert.ToDouble(data[0]));
                        inputs.Add(System.Convert.ToDouble(data[1]));
                        inputs.Add(System.Convert.ToDouble(data[2]));
                        inputs.Add(System.Convert.ToDouble(data[3]));
                        inputs.Add(System.Convert.ToDouble(data[4]));

                        double o1 = Map(0, 1, -1, 1, System.Convert.ToSingle(data[5]));
                        outputs.Add(o1);
                        double o2 = Map(0, 1, -1, 1, System.Convert.ToSingle(data[6]));
                        outputs.Add(o2);

                        calcOutputs = _ann.Train(inputs, outputs);
                        thisError = ((Mathf.Pow((float)(outputs[0] - calcOutputs[0]), 2) +
                            Mathf.Pow((float)(outputs[1] - calcOutputs[1]), 2))) / 2.0f;
                    }
                    _sse += thisError;
                }
                _trainingProgress = (float)i / (float) (epochs);
                _sse /= lineCount;

                // If sse is not better then reload previous set of weights and decrease alpha
                if (_lastSEE < _sse)
                {
                    _ann.LoadWeights(currentWeights);
                    _ann.alpha = Mathf.Clamp((float) _ann.alpha - 0.001f, 0.01f, 0.9f);
                }
                else // Increase alpha
                {
                    _ann.alpha = Mathf.Clamp((float)_ann.alpha + 0.001f, 0.01f, 0.9f);
                    _lastSEE = _sse;
                }
                yield return null;
            }
        }
        _trainingDone = true;
        SaveWeightsToFile();
    }

    void SaveWeightsToFile()
    {
        string path = Application.dataPath + "/weights.txt";
        StreamWriter streamWriter = File.CreateText(path);

        streamWriter.WriteLine(_ann.PrintWeights());
        streamWriter.Close();
    }

    void LoadWeightsFromFile()
    {
        string path = Application.dataPath + "/weights.txt";
        StreamReader streamReader = File.OpenText(path);

        if (File.Exists(path))
        {
            string line = streamReader.ReadLine();
            _ann.LoadWeights(line);
        }
    }

    float Map(float newFrom, float newTo, float originFrom, float originTo, float value)
    {
        if (value <= originFrom)
        {
            return newFrom;
        }
        else if (value >= originTo)
        {
            return newTo;
        }
        return (newTo - newFrom) * ((value - originFrom) / (originTo - originFrom)) + newFrom;
    }

    float Round(float x)
    {
        return (float)System.Math.Round(x, System.MidpointRounding.AwayFromZero) / 2.0f;
    }

    private void OnGUI()
    {
        GUI.Label(new Rect(25, 25, 250, 30), "SEE: " + _lastSEE);
        GUI.Label(new Rect(25, 40, 250, 30), "Alpha: " + _ann.alpha);
        GUI.Label(new Rect(25, 55, 250, 30), "Trained: " + _trainingProgress);
    }

    private void Update()
    {
        if (!_trainingDone)
        {
            return;
        }

        List<double> calcOutputs = new List<double>();
        List<double> inputs = new List<double>();
        List<double> outputs = new List<double>();

        // Raycasts
        RaycastHit hit;
        float fDist = 0, rDist = 0, lDist = 0, r45Dist = 0, l45Dist = 0;

        // Forward
        if (Physics.Raycast(transform.position, this.transform.forward, out hit, visibleDistance))
        {
            fDist = 1 - Round(hit.distance / visibleDistance);
        }

        // Rigth
        if (Physics.Raycast(transform.position, this.transform.right, out hit, visibleDistance))
        {
            rDist = 1 - Round(hit.distance / visibleDistance);
        }

        // Left
        if (Physics.Raycast(transform.position, -this.transform.right, out hit, visibleDistance))
        {
            lDist = 1 - Round(hit.distance / visibleDistance);
        }

        //right 45
        if (Physics.Raycast(transform.position, Quaternion.AngleAxis(-45, Vector3.up) * this.transform.right, out hit, visibleDistance))
        {
            r45Dist = 1 - Round(hit.distance / visibleDistance);
        }

        //left 45
        if (Physics.Raycast(transform.position, Quaternion.AngleAxis(45, Vector3.up) * -this.transform.right, out hit, visibleDistance))
        {
            l45Dist = 1 - Round(hit.distance / visibleDistance);
        }

        inputs.Add(fDist);
        inputs.Add(rDist);
        inputs.Add(lDist);
        inputs.Add(r45Dist);
        inputs.Add(l45Dist);
        outputs.Add(0);
        outputs.Add(0);

        calcOutputs = _ann.CalcOutput(inputs, outputs);

        float translationInput = Map(-1, 1, 0, 1, (float)calcOutputs[0]);
        float rotationInput = Map(-1, 1, 0, 1, (float)calcOutputs[1]);

        translation = translationInput * speed * Time.deltaTime;
        rotation = rotationInput * rotationSpeed * Time.deltaTime;

        this.transform.Translate(0, 0, translation);
        this.transform.Rotate(0, rotation, 0);
    }
}
