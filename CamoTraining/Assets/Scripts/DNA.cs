﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA : MonoBehaviour
{
    // gene for colour
    public float r;
    public float g;
    public float b;
    public float size;
    public float timeToDie = 0;

    private bool _dead = false;
    private SpriteRenderer _spriteRenderer;
    private Collider2D _collider2D;

    // Start is called before the first frame update
    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<Collider2D>();
        _spriteRenderer.color = new Color(r, g, b);
        this.transform.localScale = new Vector3(size, size, size);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        _dead = true;
        timeToDie = PopulationManager.elapsed;
        _spriteRenderer.enabled = false;
        _collider2D.enabled = false;
    }
}
