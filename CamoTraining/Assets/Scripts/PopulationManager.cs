﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PopulationManager : MonoBehaviour
{
    public GameObject personPrefab;
    public int populationSize = 10;

    public static float elapsed = 0.0f;

    private int _trialTime = 10;
    private int _generation = 1;
    private List<GameObject> _population = new List<GameObject>();
    private GUIStyle _guiStyle = new GUIStyle();

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < populationSize; i++)
        {
            Vector3 position = new Vector3(Random.Range(-9, 9), Random.Range(-4.5f, 4.5f), 0);
            GameObject go = Instantiate(personPrefab, position, Quaternion.identity);

            go.GetComponent<DNA>().r = Random.Range(0.0f, 1.0f);
            go.GetComponent<DNA>().g = Random.Range(0.0f, 1.0f);
            go.GetComponent<DNA>().b = Random.Range(0.0f, 1.0f);

            go.GetComponent<DNA>().size = Random.Range(0.1f, 0.3f);

            _population.Add(go);
        }
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if(elapsed > _trialTime)
        {
            BreedNewPopulation();
            elapsed = 0;
        }
    }

    private void OnGUI()
    {
        _guiStyle.fontSize = 50;
        _guiStyle.normal.textColor = Color.white;
        GUI.Label(new Rect(10, 10, 100, 20), "Generation: " + _generation, _guiStyle);
        GUI.Label(new Rect(10, 65, 100, 20), "Trial time: " + (int)elapsed, _guiStyle);
    }

    private void BreedNewPopulation()
    {
        List<GameObject> newPopulation = new List<GameObject>();
        // get rid of individuals
        List<GameObject> sortedList = _population.OrderByDescending(o => o.GetComponent<DNA>().timeToDie).ToList();

        _population.Clear();

        // breed upper half of sorted list
        for(int i = (int) (sortedList.Count / 2.0f) - 1; i < sortedList.Count - 1; i++)
        {
            _population.Add(Breed(sortedList[i], sortedList[i+1]));
            _population.Add(Breed(sortedList[i + 1], sortedList[i]));
        }

        // destroy all parents
        for(int i = 0; i < sortedList.Count; i++)
        {
            Destroy(sortedList[i]);
        }
        _generation++;
    }

    private GameObject Breed(GameObject parent1, GameObject parent2)
    {
        Vector3 position = new Vector3(Random.Range(-9, 9), Random.Range(-4.5f, 4.5f), 0);
        GameObject offSpring = Instantiate(personPrefab, position, Quaternion.identity);

        DNA dna1 = parent1.GetComponent<DNA>();
        DNA dna2 = parent2.GetComponent<DNA>();

        // Addiding mutations into the code
        if(Random.Range(0,1000) > 5)
        {
            // swap parent DNA
            offSpring.GetComponent<DNA>().r = Random.Range(0, 10) < 5 ? dna1.r : dna2.r;
            offSpring.GetComponent<DNA>().g = Random.Range(0, 10) < 5 ? dna1.r : dna2.r;
            offSpring.GetComponent<DNA>().b = Random.Range(0, 10) < 5 ? dna1.r : dna2.r;

            offSpring.GetComponent<DNA>().size = Random.Range(0, 10) < 5 ? dna1.size : dna2.size;
        }
        else
        {
            offSpring.GetComponent<DNA>().r = Random.Range(0.0f, 1.0f);
            offSpring.GetComponent<DNA>().g = Random.Range(0.0f, 1.0f);
            offSpring.GetComponent<DNA>().b = Random.Range(0.0f, 1.0f); 

            offSpring.GetComponent<DNA>().size = Random.Range(0.1f, 0.3f);

            Debug.Log("Mutation");
        }


        return offSpring;
    }
}
